package com.disktrack.disktrack;

//Developed by Joshua Padilla 
//Austin TX: jprogramtech@hotmail.com


import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings.Global;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class NewGameActivity extends Activity implements OnItemSelectedListener{
	
	private static Button newPlayerButton;
	private static Button newGameButton;
	private static EditText playerName;
	private static ListView myPlayerListView;
	private static Spinner courseSpinner;
	private static DiscTrackDataBaseAdapter discDataHelper;
	private static ArrayList<String> playerList = new ArrayList<String>();
	private ArrayList<Player> selectedPlayersList = new ArrayList<Player>();
	Game playerGame;
	private static String courseTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_game);
		
		// Must send DataBaseHelper your current context so it knows what activity its interacting with 
		discDataHelper = new DiscTrackDataBaseAdapter(this);
		
		// Creating edit text object to access new player name value 
		playerName = (EditText)findViewById(R.id.editTextCreatePlayer);
		
		// Actually grabbing newplayer name and setting value to newPlayer string value
		String newPlayer = playerName.getText().toString();
		
		
		// display current list of players saved in database below 
		// create new player option. This will allow user to select 
		// players for new game 
		myPlayerListView = (ListView) findViewById(R.id.playerListView);
		myPlayerListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		playerList = discDataHelper.getAllData();
		
		// Must overide Simple List item select View in order to be presented as black on  the screen
		ArrayAdapter<String> adapter=new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_multiple_choice,playerList){
			 @Override
			    public View getView(int position, View convertView, ViewGroup parent) {
			        View view = super.getView(position, convertView, parent);
			        TextView text = (TextView) view.findViewById(android.R.id.text1);
			        text.setTextColor(Color.BLACK);
			        return view;
			    }
		};
		myPlayerListView.setAdapter(adapter);
		
		
		
		// Creating newPlayer button object and setting onClick listener to 
		// Add player into current database;
		newPlayerButton = (Button) findViewById(R.id.addNewPlayerButton);
		newPlayerButton.setOnClickListener(new View.OnClickListener() {
			
			@Override	
			public void onClick(View v) {
				// getting new player name to add to database 
				String newPlayer = playerName.getText().toString();
				
				long id = discDataHelper.insertData(newPlayer);
				boolean INSERT_FAILED = (id < 0);
				if(INSERT_FAILED)
				{
					Message.message(getApplicationContext(), "Unsuccessful");
				}
				else
				{
					Message.message(getApplicationContext(), "Successfully created Player");
					
					// get updated list of players stored in database to display on screen 
					// This is called again once user a new user is created 
					playerList = discDataHelper.getAllData(); 
					ArrayAdapter<String> adapter=new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_multiple_choice,playerList){
						 @Override
						    public View getView(int position, View convertView, ViewGroup parent) {
						        View view = super.getView(position, convertView, parent);
						        TextView text = (TextView) view.findViewById(android.R.id.text1);
						        text.setTextColor(Color.BLACK);
						        return view;
						    }
					};
					myPlayerListView.setAdapter(adapter);
					
				}	
				
			}
			
		});
	
		newGameButton = (Button)findViewById(R.id.beginNewGameButton);
		newGameButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				// Must use sparse Boolean Array to determine if Item has been checked 
				SparseBooleanArray positions = myPlayerListView.getCheckedItemPositions();
				for (int i = 0; i< myPlayerListView.getCount(); i++){
					if (positions.get(i))
					{
						Player player = new Player((String) myPlayerListView.getAdapter().getItem(i));
						selectedPlayersList.add(player);
					}
				}
				
				playerGame = new Game(selectedPlayersList);
				
				Bundle bundle = new Bundle();
				bundle.putSerializable("Game", playerGame);
				startActivity(new Intent(getApplicationContext(), HandiCapActivity.class).putExtra("selectedPlayersList", selectedPlayersList).putExtras(bundle).putExtra("courseTitle",courseTitle));
				//putExtra("selectedPlayersList", selectedPlayersList)
			}
		});
		
		courseSpinner = (Spinner) findViewById(R.id.spinner1); 
		
		ArrayAdapter spinnerAdapter =ArrayAdapter.createFromResource(this,R.array.courses, android.R.layout.simple_spinner_item);
		courseSpinner.setAdapter(spinnerAdapter);
		courseSpinner.setOnItemSelectedListener(this);
		
	}

	
	
//////////////////////////////////////////////////////////////////////////////////////////////////////// Everything below this line
//////////////////////////////////////////////////////////////////////////////////////////////////////// is created by android 
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_game, menu);
		menu.add("Get Selected");
		return true;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item){
		SparseBooleanArray positions = myPlayerListView.getCheckedItemPositions();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i< myPlayerListView.getCount(); i++){
			if (positions.get(i))
			{
				sb.append(myPlayerListView.getAdapter().getItem(i) + "\n");
			}
		}
		Toast.makeText(this,sb, Toast.LENGTH_LONG).show();
		return true;
		
	}
	
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		TextView myText = (TextView) view;
		Toast.makeText(this, "You selected" + myText.getText().toString(), Toast.LENGTH_SHORT).show();
		courseTitle = myText.getText().toString();
		
	}



	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}


}
