package com.disktrack.disktrack;

import java.util.ArrayList;

import com.disktrack.disktrack.DiscTrackDataBaseAdapter.DiscHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DiscCourseDataBaseAdapter {
	
	CourseHelper courseHelper;
	
	public DiscCourseDataBaseAdapter(Context context){
		
		courseHelper = new CourseHelper(context); 
		
	}
	
	public long insertData(String name, ArrayList<String> parPerHoleList)
	{
		ArrayList<String> parPerHole = new ArrayList<String>();
		parPerHole = parPerHoleList;
		
		// db created as database object to write new data to database 
		// all new values inserted into table must be placed within in ContentValues 
		// object using the put command. First parameter is field second parameter is value. 
		// if sql command is successful it will return a value greater then one 
		SQLiteDatabase db = courseHelper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(CourseHelper.NAME, name);
		for(String par: parPerHoleList){
			contentValues.put(CourseHelper.NAME, Integer.parseInt(par));
		}
		long id=db.insert(CourseHelper.TABLE_NAME, null, contentValues);
		return id;
	}
	
	static class CourseHelper extends SQLiteOpenHelper{

		private Context context;
		private static final String DATABASE_NAME = "coursedatabase";
		private static final String TABLE_NAME = "COURSETABLE";
		private static final int DATABASE_VERSION = 1;  
		private static final String UID = "_id";
		private static final String NAME = "Name";
		private static final String HOLE1 = "Hole1";
		private static final String HOLE2 = "Hole2";
		private static final String HOLE3 = "Hole3";
		private static final String HOLE4 = "Hole4";
		private static final String HOLE5 = "Hole5";
		private static final String HOLE6 = "Hole6";
		private static final String HOLE7 = "Hole7";
		private static final String HOLE8 = "Hole8";
		private static final String HOLE9 = "Hole9";
		private static final String HOLE10 = "Hole10";
		private static final String HOLE11 = "Hole11";
		private static final String HOLE12 = "Hole12";
		private static final String HOLE13 = "Hole13";
		private static final String HOLE14 = "Hole14";
		private static final String HOLE15 = "Hole15";
		private static final String HOLE16 = "Hole16";
		private static final String HOLE17 = "Hole17";
		private static final String HOLE18 = "Hole18";
		
		private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
				+ " (" + NAME + " VARCHAR(255) PRIMARY KEY, " + HOLE1 + "INTEGER DEFAULT NULL"+ ");";
		
		private static final String DROP_TABLE = "DROP TABLE IF EXISTS "
				+ TABLE_NAME;
			
		public CourseHelper(Context context)
		{
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			this.context = context;
		}


		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			
			try {
				db.execSQL(CREATE_TABLE);
				Message.message(context, "onCreate called DiscCourseDB");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				Message.message(context, "" + e);
				Log.i("DataBase Error", "Unable to create database");
			}
			
		}


		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			
			try {

//				Message.message(context, "onUpgrade called");
				db.execSQL(DROP_TABLE);
				onCreate(db);
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				Message.message(context, "" + e);
			}
			
		}	
	}
}
