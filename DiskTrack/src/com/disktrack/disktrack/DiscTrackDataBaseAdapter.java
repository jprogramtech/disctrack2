package com.disktrack.disktrack;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


// Database adapter used ass bridge between 
// app and actual database to run sql commands 
// such as insert, delete, query and more. 
public class DiscTrackDataBaseAdapter {
	
	DiscHelper helper;
	public DiscTrackDataBaseAdapter(Context context){
		
		// create helper object to access onCreate and upgrade methods of inner class.  
		helper = new DiscHelper(context);
		
	}
	
	public long insertData(String name)
	{
		
		// db created as database object to write new data to database 
		// all new values inserted into table must be placed within in ContentValues 
		// object using the put command. First parameter is field second parameter is value. 
		// if sql command is successful it will return a value greater then one 
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(DiscHelper.NAME, name);
		long id=db.insert(DiscHelper.TABLE_NAME, null, contentValues);
		return id;
	}
	
	public ArrayList<String> getAllData()
	{
		SQLiteDatabase db=helper.getWritableDatabase();	
		
		String[] columns ={DiscHelper.UID,DiscHelper.NAME};
		Cursor cursor =db.query(DiscHelper.TABLE_NAME, columns, null,null, null, null, null);
		ArrayList<String> playerList = new ArrayList<String>();
		while(cursor.moveToNext())
		{
			
			int index = cursor.getColumnIndex(DiscHelper.NAME);
			String name=cursor.getString(index);
			playerList.add(name);
			
		}
		
		return playerList;
	}
	
	public List<String> getData(String name)
	{
		SQLiteDatabase db=helper.getWritableDatabase();
		String[] columns ={DiscHelper.NAME};
		Cursor cursor =db.query(DiscHelper.TABLE_NAME, columns, DiscHelper.NAME + " = '"+name+"'",
				null, null, null, null);
		
		ArrayList<String> playerList = new ArrayList<String>();
		while(cursor.moveToNext())
		{	
			int index1=cursor.getColumnIndex(DiscHelper.NAME);
			String personName=cursor.getString(index1);
			playerList.add(personName);
		}
		return playerList;
		
	}
	// Inner data base class used to create and 
	//  update date base/ created as static class because 
	// dont want any other class having access to member 
	// variables
	static class DiscHelper extends SQLiteOpenHelper {
		
		private static final String DATABASE_NAME = "discdatabase";
		private static final String TABLE_NAME = "DISCTABLE";
		private static final int DATABASE_VERSION = 113;  
		private static final String UID = "_id";
		private static final String NAME = "Name";
		private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME
				+ " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + NAME
				+ " VARCHAR(255));";
		private static final String DROP_TABLE = "DROP TABLE IF EXISTS "
				+ TABLE_NAME;
		private Context context;
		
		public DiscHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			this.context = context;
		//	Message.message(context, "constructor called");
			
		}

	
		@Override
		public void onCreate(SQLiteDatabase db) {
			// CREATE A DATABASE

			try {
				db.execSQL(CREATE_TABLE);
//				Message.message(context, "onCreate called");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				Message.message(context, "" + e);
				Log.i("DataBase Error", "Unable to create database");
			}

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

			try {

//				Message.message(context, "onUpgrade called");
				db.execSQL(DROP_TABLE);
				onCreate(db);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				Message.message(context, "" + e);
			}

		}

	}

	
	
	
	
	

}
