package com.disktrack.disktrack;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter.LengthFilter;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class Hole1Activty extends Activity {
	
	// Initialize Button and Edit Text variables 
	protected static Button nextButton;
	protected static Button prevButton;
	protected static TextView playerOneName;
	protected static TextView playerTwoName;
	protected static TextView playerThreeName;
	protected static TextView playerFourName;
	protected static TextView playerFiveName;
	protected static TextView courseTitle;
	protected static EditText playerOneEditText;
	protected static EditText playerTwoEditText;
	protected static EditText playerThreeEditText;
	protected static EditText playerFourEditText;
	protected static EditText playerFiveEditText;
	protected static Button viewGameScoreButton;
	protected static int holeCount =1;
	protected static ArrayList<Player> selectedPlayersList;
	protected static String courseName;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hole1_activty);
		
	
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		
	}


	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_hole1_activty,
					container, false);
			setHasOptionsMenu(true);
			
			
			// Must access current parent activity 
			// so you can access passed objects 
			Activity myActivity = getActivity();
			Intent myIntent = myActivity.getIntent(); // Intents allow you grabe objects from passed activities 
			Game playerGame = (Game)myIntent.getSerializableExtra("Game"); // grabbing Game object from passed activity
			@SuppressWarnings("unchecked")
			ArrayList<Player> selectedPlayersList = (ArrayList<Player>)myIntent.getSerializableExtra("selectedPlayersList"); 
			courseName = (String)myIntent.getSerializableExtra("courseTitle");
			
			// Initialize course title and set name
			courseTitle = (TextView) rootView.findViewById(R.id.courseTitle);
			courseTitle.setText(courseName);
		
			// initializing game Score Edit Text fields 
			playerOneEditText = (EditText)rootView.findViewById(R.id.playerOneEditScore);
			playerTwoEditText = (EditText)rootView.findViewById(R.id.playerTwoEditScore);
			playerThreeEditText = (EditText)rootView.findViewById(R.id.playerThreeEditScore);
			playerFourEditText = (EditText)rootView.findViewById(R.id.playerFourEditText);
			playerFiveEditText = (EditText)rootView.findViewById(R.id.playerFiveEditText);
			
			// setting all other Edit Text visibility to Invisible 
			// unless other wise needed. 
			playerTwoEditText.setVisibility(View.INVISIBLE);
			playerThreeEditText.setVisibility(View.INVISIBLE);
			playerFourEditText.setVisibility(View.INVISIBLE);
			playerFiveEditText.setVisibility(View.INVISIBLE);
			
			
				
			// Initializing Text View For player names 		
			playerOneName = (TextView) rootView.findViewById(R.id.playerOneName);
			playerTwoName = (TextView) rootView.findViewById(R.id.playerTwoName);
			playerThreeName = (TextView) rootView.findViewById(R.id.playerThreeName);
			playerFourName = (TextView) rootView.findViewById(R.id.playerFourName);
			playerFiveName = (TextView) rootView.findViewById(R.id.playerFiveName);
			
			// setting all other players visibility to Invisible 
			// unless other wise needed 
			playerTwoName.setVisibility(View.INVISIBLE);
			playerThreeName.setVisibility(View.INVISIBLE);
			playerFourName.setVisibility(View.INVISIBLE);
			playerFiveName.setVisibility(View.INVISIBLE);
			
			
			// Set players names for game and set up 
			// and set up edit score field for each player
			 editNameAndScoreFields(selectedPlayersList);
				
			
			 
			 // next button takes care of all game logic 
			 
			nextButton = (Button) rootView.findViewById(R.id.nextButton);
			nextButton.setOnClickListener(new View.OnClickListener() {
			
				/// Must call activity 
				/// in order to get game arrayList of players 
				/// so your inner annonymous classes can have access to 
				// playersArrayList
				Activity myActivity = getActivity();
				Intent myIntent = myActivity.getIntent();
				Game playerGame = (Game)myIntent.getSerializableExtra("Game"); // grabbing Game object from passed activity
				@SuppressWarnings("unchecked")
				
				List<Player> selectedPlayersList = playerGame.playListOfPlayers();
				
				
				@Override
				public void onClick(View v) {
				
				// create players score to hold players current hole score 
				ArrayList<Integer> playerScores = new ArrayList<Integer>(); // should be recreated with every Button push 
				int numberOfPlayers = selectedPlayersList.size(); // getting number of players to determine what edit fields to show
				
				
				if (numberOfPlayers == 1)
				{
			
					playerScores.add(Integer.parseInt(playerOneEditText.getText().toString()));
					
				}else if(selectedPlayersList.size() == 2) 
				{
					
					playerScores.add(Integer.parseInt(playerOneEditText.getText().toString()));
					playerScores.add(Integer.parseInt(playerTwoEditText.getText().toString()));
					
				
				}
				else if (selectedPlayersList.size() ==3)
				{
					
					playerScores.add(Integer.parseInt(playerOneEditText.getText().toString()));
					playerScores.add(Integer.parseInt(playerTwoEditText.getText().toString()));
					playerScores.add(Integer.parseInt(playerThreeEditText.getText().toString()));
					
					
					
				}else if(selectedPlayersList.size() == 4)
				{
					
					playerScores.add(Integer.parseInt(playerOneEditText.getText().toString()));
					playerScores.add(Integer.parseInt(playerTwoEditText.getText().toString()));
					playerScores.add(Integer.parseInt(playerThreeEditText.getText().toString()));
					playerScores.add(Integer.parseInt(playerFourEditText.getText().toString()));
					
					
				}
				
				else if (selectedPlayersList.size() == 5)
				{
					
					playerScores.add(Integer.parseInt(playerOneEditText.getText().toString()));
					playerScores.add(Integer.parseInt(playerTwoEditText.getText().toString()));
					playerScores.add(Integer.parseInt(playerThreeEditText.getText().toString()));
					playerScores.add(Integer.parseInt(playerFourEditText.getText().toString()));
					playerScores.add(Integer.parseInt(playerFiveEditText.getText().toString()));
					
				}
					
				if(holeCount ==18)
				{
					Toast.makeText(getActivity(), "Your at hole 18", Toast.LENGTH_LONG).show();
					// New Activity will be placed here to 
						
				}
				else
				{
					for(int i = 0; i < selectedPlayersList.size();i++)
						{
							// setting each individual players score per hole
					//		Toast.makeText(getActivity(), "Entering for loop to set player scores", Toast.LENGTH_SHORT).show();
							selectedPlayersList.get(i).setScore(playerScores.get(i));
						//	Toast.makeText(getActivity(), Integer.toString(selectedPlayersList.get(i).getTotalAmountOfScoresSize()), Toast.LENGTH_LONG).show();
							
						}
					}
					getActivity().setTitle("Hole " + (holeCount +1));
					//Toast.makeText(getActivity(),Integer.toString(playerScores.get(0)), Toast.LENGTH_LONG).show();
					editNameAndScoreFields(selectedPlayersList);
					holeCount++;
				}
				
			});
			
			
			
			prevButton = (Button) rootView.findViewById(R.id.previousButton);
			prevButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					startActivity(new Intent(getActivity(), NewGameActivity.class));
					
				}
			});
			
			viewGameScoreButton = (Button) rootView.findViewById(R.id.viewScore);
			viewGameScoreButton.setOnClickListener(new View.OnClickListener() {
				
				
				@Override
				public void onClick(View v) {
					
					
					Activity i = getActivity();
					Intent myIntent = i.getIntent();
					Game playerGame = (Game)myIntent.getSerializableExtra("Game");
					
					Bundle bundle = new Bundle();
					bundle.putSerializable("Game", playerGame);
					startActivity(new Intent(getActivity(), GameScoreActivity.class).putExtras(bundle).putExtra("Hole",holeCount));
					
					
					//Toast.makeText(getActivity(), Integer.toString(holeCount), Toast.LENGTH_LONG).show();
		
				}
			});	
			
			return rootView;
		}
		
		private void editNameAndScoreFields(List<Player> selectedPlayersList)
		{
			if (selectedPlayersList.size() == 1)
			{
		
				playerOneName.setText(selectedPlayersList.get(0).toString());
				
			}else if(selectedPlayersList.size() == 2) 
			{
				
				playerOneEditText.setVisibility(View.VISIBLE);
				playerOneEditText.setText("");
				playerOneName.setText(selectedPlayersList.get(0).toString());
				playerTwoEditText.setVisibility(View.VISIBLE);
				playerTwoEditText.setText("");
				playerTwoName.setText(selectedPlayersList.get(1).toString());
				playerTwoName.setVisibility(View.VISIBLE);
				
			
			}
			else if (selectedPlayersList.size() ==3)
			{
				
				playerOneEditText.setVisibility(View.VISIBLE);
				playerOneEditText.setText("");
				playerOneName.setText(selectedPlayersList.get(0).toString());
				playerTwoEditText.setVisibility(View.VISIBLE);
				playerTwoEditText.setText("");
				playerTwoName.setText(selectedPlayersList.get(1).toString());
				playerTwoName.setVisibility(View.VISIBLE);
				playerThreeEditText.setVisibility(View.VISIBLE);
				playerThreeEditText.setText("");
				playerThreeName.setText(selectedPlayersList.get(2).toString());
				playerThreeName.setVisibility(View.VISIBLE);
				
				
				
			}else if(selectedPlayersList.size() == 4)
			{
				
				playerOneEditText.setVisibility(View.VISIBLE);
				playerOneEditText.setText("");
				playerOneName.setText(selectedPlayersList.get(0).toString());
				playerTwoEditText.setVisibility(View.VISIBLE);
				playerTwoEditText.setText("");
				playerTwoName.setText(selectedPlayersList.get(1).toString());
				playerTwoName.setVisibility(View.VISIBLE);
				playerThreeEditText.setVisibility(View.VISIBLE);
				playerThreeEditText.setText("");
				playerThreeName.setText(selectedPlayersList.get(2).toString());
				playerThreeName.setVisibility(View.VISIBLE);
				playerFourEditText.setVisibility(View.VISIBLE);
				playerFourEditText.setText("");
				playerFourName.setText(selectedPlayersList.get(3).toString());
				playerFourName.setVisibility(View.VISIBLE);
				
				
			}
			
			else if (selectedPlayersList.size() == 5)
			{
				
				playerOneEditText.setVisibility(View.VISIBLE);
				playerOneEditText.setText("");
				playerOneName.setText(selectedPlayersList.get(0).toString());
				playerTwoEditText.setVisibility(View.VISIBLE);
				playerTwoEditText.setText("");
				playerTwoName.setText(selectedPlayersList.get(1).toString());
				playerTwoName.setVisibility(View.VISIBLE);
				playerThreeEditText.setVisibility(View.VISIBLE);
				playerThreeEditText.setText("");
				playerThreeName.setText(selectedPlayersList.get(2).toString());
				playerThreeName.setVisibility(View.VISIBLE);
				playerFourEditText.setVisibility(View.VISIBLE);
				playerFourEditText.setText("");
				playerFourName.setText(selectedPlayersList.get(3).toString());
				playerFourName.setVisibility(View.VISIBLE);
				playerFiveName.setText(selectedPlayersList.get(4).toString());
				playerFiveName.setVisibility(View.VISIBLE);
				playerFiveEditText.setVisibility(View.VISIBLE);
				playerFiveEditText.setText("");
				
			}
		}
		
		
	}
	
	
	
	// Everything below this line is self generated by android
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.hole1_activty, menu);
		menu.add("Get Game Score");
		return true;
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	

}
