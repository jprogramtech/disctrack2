package com.disktrack.disktrack;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.os.Build;

public class GameScoreActivity extends Activity {
	
	protected static ListView playerScoreListView;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_score);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game_score, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_game_score,
					container, false);
			
			
			// list that will be used to 
			ArrayList<String> playerScores = new ArrayList<String>();
			
			
			Activity myActivity = getActivity();
			Intent myIntent = myActivity.getIntent();
			Game playerGame = (Game)myIntent.getSerializableExtra("Game");
			int hole = myIntent.getIntExtra("Hole",0);
			
			
			
			Toast.makeText(getActivity(), Integer.toString(hole), Toast.LENGTH_LONG).show();
			
			for(int i = 0; i< (hole-1); i++)
			{
				for(int j = 0; j< playerGame.getNumberOfPlayers(); j++)
				{
					playerScores.add("Hole " + Integer.toString(i+1) + ": " + playerGame.getPlayerName(j) + " --- " + " Score: " + playerGame.getPlayerScoreAtCurrentHole(j, i));
					
				}
			}
			
			playerScores.add("OverAll Score: "  + "\n\n Josh Score (HandiCap): 2 " + "\n\n Eric Score(HandiCap): -8" + "\n\n Luke Score(HandiCap): -2 " + "\n\nEric Is Winning!!");
			
			playerScoreListView = (ListView)rootView.findViewById(R.id.listView1);
			
			ArrayAdapter<String> adapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,playerScores);
			
			playerScoreListView.setAdapter(adapter);
				
			
			return rootView;
		}
	}

}
