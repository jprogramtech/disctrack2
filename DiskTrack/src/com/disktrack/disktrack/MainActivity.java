package com.disktrack.disktrack;

// Developed by Joshua Padilla 
// Austin TX: jprogramtech@hotmail.com

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	protected static Button mNewGameButton;
	protected static Button mCoursesGameButton;
	protected static Button mPlayersButton;
	protected static Button mNextHoleButton;
	protected static ImageView androidPic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
// used to inflate fragment.main.xml 
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    
    // start of interaction with fragment_main 
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,	
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            
            
            androidPic = (ImageView)rootView.findViewById(R.id.imageView1);
            androidPic.setMaxHeight(300);
            androidPic.setMaxWidth(300);
            // button used to start new game if clicked New game Activity is called 
            // New game activity will allow you to select players and start game 
            mNewGameButton = (Button) rootView.findViewById(R.id.newGameButton);
            mNewGameButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					startActivity(new Intent(getActivity(),NewGameActivity.class));
					
				}
			});
            
            mCoursesGameButton = (Button) rootView.findViewById(R.id.coursesButton);
            mCoursesGameButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					startActivity(new Intent(getActivity(), CoursesHomeActivity.class));
					
				}
			});
   
            return rootView;
        }
    }

}
