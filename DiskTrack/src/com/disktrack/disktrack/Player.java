package com.disktrack.disktrack;

import java.io.Serializable;
import java.util.ArrayList;


@SuppressWarnings("serial")
public class Player implements Serializable {
	
	private String name;
	private ArrayList<Integer> playerScore;
	
	public Player(String name){
		
		this.name = name;
		this.playerScore = new ArrayList<Integer>();
	}
	
	public String getFname()
	{
		return this.name;
	}
	

	public ArrayList<Integer> getGameScore(){
		
		return this.playerScore;
	}
	
	public Integer getHoleScore(int hole)
	{
		return this.playerScore.get(hole);
	}
	
	public void setScore(int score)
	{
		this.playerScore.add(score);
	}
	
	public Integer getTotalAmountOfScoresSize()
	{
		return this.playerScore.size();
	}
	
	
	@Override
	public String toString()
	{
		return this.name;
	}
	
	

}
