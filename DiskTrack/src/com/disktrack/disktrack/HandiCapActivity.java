package com.disktrack.disktrack;

import java.util.ArrayList;

import org.w3c.dom.Text;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

public class HandiCapActivity extends Activity {
	
	//private static int joshScore[] = new int[]  {55,60,62,50,50,54,63,62,70,54,55,56,59,60,61,55,60,61,63,52,50,49,48,55,55,51,60,50,63,54};
	//private static int ericScore[] = new int[]  {60,60,55,44,55,77,62,74,72,71,72,73,74,75,77,68,66,60,60,55,44,55,77,62,74,72,71,72,73,79};
	//private static int lukeScore[] = new int[]  {48,55,55,51,60,50,63,54,72,71,72,73,74,75,54,55,60,70,85,90,72,75,73,74,78,55,64,60,61,63};
	private static Button acceptHandicapButton;
	private static Button beginGameButton;
	private static String courseName;
	private static boolean handiCapAccept = false;
	private static TextView firstPlayerName;
	private static TextView secondPlayerName;
	private static TextView thirdPlayerName;
	private static TextView playerOneHandicapScore;
	private static TextView playerTwoHandicapScore;
	private static TextView playerThreeHandicapScore;
	private static int joshHandiCapScore = 0;
	private static int ericHandiCapScore = -10;
	private static int lukeHandiCapScore = -4;
	private static TextView handiCapTitle;
	

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_handi_cap);
		
		
		// must accept passed objects from passed activity
		Intent myIntent = getIntent(); // Intents allow you grabe objects from passed activities 
		Game playerGame = (Game)myIntent.getSerializableExtra("Game"); // grabbing Game object from passed activity
		@SuppressWarnings("unchecked")
		ArrayList<Player> selectedPlayersList = (ArrayList<Player>)myIntent.getSerializableExtra("selectedPlayersList"); 
		courseName = (String)myIntent.getSerializableExtra("courseTitle");
		
		//Toast.makeText(this,Integer.toString(selectedPlayersList.size()), Toast.LENGTH_LONG).show();
		
		handiCapTitle = (TextView) findViewById(R.id.handiCapTextView);
		handiCapTitle.setText("Handicap System");
		
		
		firstPlayerName = (TextView) findViewById(R.id.firstPlayerTextViewHandiCap);
		secondPlayerName = (TextView) findViewById(R.id.secondPlayerTextView);
		thirdPlayerName = (TextView) findViewById(R.id.thirdPlayerTextView);
		playerOneHandicapScore = (TextView) findViewById(R.id.playerOneHandiScore);
		playerTwoHandicapScore = (TextView) findViewById(R.id.playerTwoHandiScore);
		playerThreeHandicapScore = (TextView) findViewById(R.id.playerThreeHandiScore);
		
		
		if (selectedPlayersList.size() > 2)
		{
			firstPlayerName.setText(selectedPlayersList.get(0).getFname());
			secondPlayerName.setText(selectedPlayersList.get(1).getFname());
			thirdPlayerName.setText(selectedPlayersList.get(2).getFname());
			playerOneHandicapScore.setText(Integer.toString(joshHandiCapScore));
			playerTwoHandicapScore.setText(Integer.toString(ericHandiCapScore));
			playerThreeHandicapScore.setText(Integer.toString(lukeHandiCapScore));
		}
		else if (selectedPlayersList.size() == 2)
		{
			firstPlayerName.setText(selectedPlayersList.get(0).getFname());
			secondPlayerName.setText(selectedPlayersList.get(1).getFname());
			playerOneHandicapScore.setText(Integer.toString(joshHandiCapScore));
			playerTwoHandicapScore.setText(Integer.toString(ericHandiCapScore));
		}
		else
		{
			firstPlayerName.setText(selectedPlayersList.get(0).getFname());
			playerOneHandicapScore.setText(Integer.toString(joshHandiCapScore));
		}
		
		
		acceptHandicapButton = (Button)findViewById(R.id.handicapButton);
		acceptHandicapButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				handiCapAccept = true;
				
				Toast.makeText(getApplicationContext(), "Handy Cap Accepted", Toast.LENGTH_LONG).show();
				
				
			}
		});
		
		beginGameButton = (Button) findViewById(R.id.beginGameButton);
		beginGameButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				Intent myIntent = getIntent();
				Game playerGame = (Game)myIntent.getSerializableExtra("Game"); // grabbing Game object from passed activity
				@SuppressWarnings("unchecked")
				ArrayList<Player> selectedPlayersList = (ArrayList<Player>)myIntent.getSerializableExtra("selectedPlayersList"); 
				courseName = (String)myIntent.getSerializableExtra("courseTitle");
				
				Bundle bundle = new Bundle();
				bundle.putSerializable("Game", playerGame);
				startActivity(new Intent(getApplicationContext(), Hole1Activty.class).putExtra("selectedPlayersList", selectedPlayersList).putExtras(bundle).putExtra("courseTitle",courseName));
				
				
			}
		});
		
		
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.handi_cap, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
