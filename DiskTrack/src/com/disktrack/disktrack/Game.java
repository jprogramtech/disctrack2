package com.disktrack.disktrack;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

// must place here Implementing serializable allows you 
// to pass objects from activity to activity


@SuppressWarnings("serial")
public class Game implements Serializable {
	
	/*
	private String firstPlayer;
	private String secondPlayer;
	private ArrayList<Integer> firstPlayerPoints;
	private ArrayList<Integer> secondPlayerPoints;
	*/
	private int playerCount;
	private List<Player> playersList;
	
	public Game(List<Player> playersList){
		
		this.playerCount = playersList.size();
		this.playersList = playersList;	
	}
	
	public List<Player> playListOfPlayers()
	{
		return this.playersList;
	}
	
	public int getNumberOfPlayers(){
		
		return this.playersList.size();
	}
	
	public String getPlayerName(int i)
	{
		return this.playersList.get(i).getFname();
	}
	
	public Player getPlayerObject(int i)
	{
		return this.playersList.get(i);
	}
	
	public Integer getPlayerScoreAtCurrentHole(int i, int hole)
	{
		return this.playersList.get(i).getHoleScore(hole);
	}
	
	/*
	
	public Integer returnPlayerScoreSizeAtIndex()
	{
		return this.playersList.get(0).getTotalAmountOfScoresSize();
	}

*/	
	public ArrayList<String> gameScoreCalculation(int hole){
		ArrayList<String>gameScore = new ArrayList<String>();
		
		
		for(int i = 0; i<hole; i++)
		{
			String gameHole = Integer.toString(i +1);
			for(int j = 0; j< playersList.size(); j++)
			{
				gameScore.add("Hole: " +  gameHole + "   " + playersList.get(j).getFname() + "  " + playersList.get(j).getHoleScore(i));
			}
		}
		return gameScore;
	}	
}
